package main

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
	"net/http"
	"time"
)

var upgrader = websocket.Upgrader{}

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Pre(middleware.RemoveTrailingSlash())

	e.Logger.SetLevel(log.DEBUG)

	e.GET("/ws", geoWebsocket)

	e.Logger.Fatal(e.Start("localhost:1337"))
}

func geoWebsocket(c echo.Context) error {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	ws, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
	if err != nil {
		return err
	}
	defer ws.Close()

	for {
		response, er := json.Marshal(time.Now().Format(time.RFC1123))
		if er != nil {
			c.Logger().Error(er)
		} else {
			c.Logger().Debug("Send: ", string(response))

			if err := ws.WriteMessage(websocket.TextMessage, []byte(response)); err != nil {
				c.Logger().Error(err)
			}
		}

		time.Sleep(1 * time.Second)
	}
}
